const nav_btn = document.querySelector(".drop-menu");
const cross_btn = document.querySelector(".cross-menu");
const nav = document.querySelector(".nav");

nav_btn.addEventListener("click", () => {
  if (nav.classList.contains("active")) {
    nav.classList.remove("active");
  } else {
    nav.classList.add("active");
    cross_btn.classList.add("active");
    nav_btn.classList.add("deactive");
  }
});

cross_btn.addEventListener("click", () => {
  if (nav.classList.contains("active")) {
    nav.classList.remove("active");
    nav_btn.classList.remove("deactive");
    cross_btn.classList.remove("active");
  } else {
    nav.classList.add("active");
  }
});
